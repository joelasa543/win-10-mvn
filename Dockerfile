# escape=`
ARG WINDOWS_DOCKER_TAG=1809
FROM openjdk:11-jdk-windowsservercore-$WINDOWS_DOCKER_TAG

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="C:/Users/ContainerAdministrator"
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN Invoke-WebRequest -Uri ${env:BASE_URL}/apache-maven-${env:MAVEN_VERSION}-bin.zip -OutFile ${env:TEMP}/apache-maven.zip ; `
  Expand-Archive -Path ${env:TEMP}/apache-maven.zip -Destination C:/ProgramData ; `
  Move-Item C:/ProgramData/apache-maven-${env:MAVEN_VERSION} C:/ProgramData/Maven ; `
  New-Item -ItemType Directory -Path C:/ProgramData/Maven/Reference | Out-Null ; `
  Remove-Item ${env:TEMP}/apache-maven.zip 

ENV MAVEN_HOME C:/ProgramData/Maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"


COPY mvn-entrypoint.ps1 C:/ProgramData/Maven/mvn-entrypoint.ps1
COPY settings-docker.xml C:/ProgramData/Maven/Reference/settings-docker.xml
RUN $content = Get-Content "C:/ProgramData/Maven/Reference/settings-docker.xml" ; `
    $content | ForEach-Object { $_ -replace '/usr/share/maven/ref/repository','C:\ProgramData\Maven\Reference\repository' } | `
    Set-Content -Path "C:/ProgramData/Maven/Reference/settings-docker.xml" ; `
    setx /M PATH $('{0};{1}' -f $env:PATH,'C:\ProgramData\Maven\bin') | Out-Null ; `
    powershell.exe -f "C:/ProgramData/Maven/mvn-entrypoint.ps1"
